import java.net.HttpURLConnection
import java.net.URL
import kotlin.concurrent.thread

fun checkWebsite(url: String): Boolean = try {
    val connection = URL(url).openConnection() as HttpURLConnection
    connection.requestMethod = "HEAD"
    connection.connectTimeout = 5000
    connection.readTimeout = 5000
    connection.responseCode == HttpURLConnection.HTTP_OK
} catch (e: Exception) {
    false
}

fun main() {
    val urlList = listOf(
        "https://www.google.com",
        "https://www.facebook.com",
        "https://www.github.com",
        "https://www.twitter.com",
        "https://www.instagram.com",
        "https://translate.yandex.ru",
        "https://www.youtube.com",
        "https://18lordserial.pro",
        "https://animego.pw",
        "https://habr.com"
    )

    urlList.forEach { url ->
        thread {
            println("Сайт $url ${if (checkWebsite(url)) "доступен" else "недоступен"}")
        }
    }
}